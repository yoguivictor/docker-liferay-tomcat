# liferay/tomcat:7.0.42
FROM liferay/base:6.2
ENV LIFERAY_HOME /opt/lportal
ENV TOMCAT_HOME ${LIFERAY_HOME}/tomcat-7.0.42
ADD bin/ /usr/local/bin/
CMD ["tomcat"]
